package quickly.quit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            int display = Integer.parseInt(preferences.getString("pref_display", "0"));
            if (display == 0) {
                Intent i = new Intent(context, DisplayNotificationService.class);
                i.putExtra("origin", "boot");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startService(i);
            } else if (display == 1) {
                Intent fbsi = new Intent(context, FloatingButtonService.class);
                context.startService(fbsi);
            } else {
                Intent i = new Intent(context, DisplayNotificationService.class);
                i.putExtra("origin", "boot");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startService(i);

                Intent fbsi = new Intent(context, FloatingButtonService.class);
                context.startService(fbsi);
            }
        }
    }
}
