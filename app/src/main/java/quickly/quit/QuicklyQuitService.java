package quickly.quit;

import android.app.IntentService;
import android.content.Intent;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.app.NotificationManager;
import android.media.AudioDeviceInfo;

public class QuicklyQuitService extends IntentService {

    public QuicklyQuitService() {
        super("QuicklyQuitService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        //lock the screen
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        dpm.lockNow();

        //go to home screen
        boolean nohome = preferences.getBoolean("pref_dont_go_home", false);
        if (!nohome) {
            Intent qq = new Intent(Intent.ACTION_MAIN);
            qq.addCategory(Intent.CATEGORY_HOME);
            qq.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(qq);
        }

        //mute the device
        boolean mute = preferences.getBoolean("pref_mute", false);
        boolean dmh = preferences.getBoolean("pref_dont_mute_headphones", false);
        if (mute && !dmh) {
            muteDevice();
        } else if (mute && dmh) {
            if (!areHeadPhonesConnected()) {
                muteDevice();
            }
        }

        //remove the notification or/and the floating button
        boolean remove = preferences.getBoolean("pref_remove_after_use", false);
        if (remove) {
            int display = Integer.parseInt(preferences.getString("pref_display", "0"));
            if (display == 0) { //remove only the notification
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.cancel(1922);
            } else if (display == 1) { //remove only the floating button
                Intent fbsi = new Intent(this, FloatingButtonService.class);
                this.stopService(fbsi);
            } else { //remove both
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.cancel(1922);
                Intent fbsi = new Intent(this, FloatingButtonService.class);
                this.stopService(fbsi);
            }
        }        
    }

    public void muteDevice() {
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //setStreamMute: deprecated in API level 23
        if (Build.VERSION.SDK_INT < 23) {
            //am.setStreamMute(AudioManager.STREAM_MUSIC, true);
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        } else {
            am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
        }
    }

    public boolean areHeadPhonesConnected() {
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        if (am == null) {
            return false;
        } else if (Build.VERSION.SDK_INT < 23) {
            return am.isWiredHeadsetOn() || am.isBluetoothScoOn() || am.isBluetoothA2dpOn();
        } else {
            AudioDeviceInfo[] devicesList = am.getDevices(AudioManager.GET_DEVICES_OUTPUTS);

            for (int i = 0; i < devicesList.length; i++) {
                AudioDeviceInfo deviceInfo = devicesList[i];

                if (deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADSET
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADPHONES
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_SCO) {
                    return true;
                }
            }
        }
        return false;
    }
}
