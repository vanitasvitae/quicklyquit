package quickly.quit;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class FloatingButtonService extends Service {

    private WindowManager windowManager;
    private ImageView floatingButton;

    private WindowManager.LayoutParams params;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        floatingButton = new ImageView(this);
        floatingButton.setImageResource(R.mipmap.ic_floating_button);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        SharedPreferences coordinates = PreferenceManager.getDefaultSharedPreferences(FloatingButtonService.this);
        int savedX = coordinates.getInt("x", 0);
        int savedY = coordinates.getInt("y", 100);
        params.x = savedX;
        params.y = savedY;

        final GestureDetector gestureDetector = new GestureDetector(this, new MyGestureDetector());
        View.OnTouchListener gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
        };

        floatingButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        floatingButton.setOnTouchListener(gestureListener);

        windowManager.addView(floatingButton, params);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != floatingButton) {
            windowManager.removeView(floatingButton);
        }
    }

    class MyGestureDetector extends android.view.GestureDetector.SimpleOnGestureListener {
        private int initialX;
        private int initialY;
        private float initialTouchX;
        private float initialTouchY;

        @Override
        public boolean onDown(MotionEvent e) {
            initialX = params.x;
            initialY = params.y;
            initialTouchX = e.getRawX();
            initialTouchY = e.getRawY();
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            params.x = initialX + (int) (e2.getRawX() - initialTouchX);
            params.y = initialY + (int) (e2.getRawY() - initialTouchY);
            windowManager.updateViewLayout(floatingButton, params);

            PreferenceManager.getDefaultSharedPreferences(FloatingButtonService.this).edit().putInt("x", params.x).commit();
            PreferenceManager.getDefaultSharedPreferences(FloatingButtonService.this).edit().putInt("y", params.y).commit();

            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Intent i = new Intent(FloatingButtonService.this, QuicklyQuitService.class);
            FloatingButtonService.this.startService(i);
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            stopSelf();
            return true;
        }
    }
}
